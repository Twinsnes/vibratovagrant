#
# Cookbook:: CoreBlog
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.
bash 'install app' do 
  code <<-EOH
    sudo apt-get update -y
    sudo apt-get install zip -y
    
    mkdir Site
    cd Site
    wget -O site.zip "https://github.com/TWinsnes/RandomStuff/raw/master/aspnetsite.zip"

    sudo unzip site.zip

    sudo dotnet restore

    sudo dotnet ef database update

    sudo mkdir /var/log/Site

    sudo chmod 777 /var/log/Site
    
    sudo echo "# Site.conf\n
start on filesystem\n
script\n
    cd /Site
    sudo dotnet restore > /var/log/Site/restore.log\n
    sudo dotnet run > /var/log/Site/run.log\n
end script" > /etc/init/Site.conf

    sudo initctl reload-configuration

    sudo service Site start
	EOH
end