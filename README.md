## About

Quick blog in .net Core running on ubuntu and PostgresSQL

## Installation

Clone repository to you local drive

If your local network runs in the 192.168.0.* range you should change the private network IP to something outside that range in the Vagrant file before continuing (line #43)

call 'Vagrant Up' to deploy

Once it has completed access webpage by opening browser to 'localhost:8080' or '192.168.0.2' (unless changed)


## Dependencies

* Vagrant
* Virtual Box

## Application Details

The application itself is stored separately to this repository. The deployable artefact is in a git repository for a lack of a better artefact repository available at the time. Source (history) can be found seperately as well.

Artefact: https://github.com/TWinsnes/RandomStuff/raw/master/aspnetsite.zip
Source: https://bitbucket.org/Twinsnes/vibratocoreblog